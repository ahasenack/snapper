snapper (0.5.6-1) unstable; urgency=medium

  * New upstream release 0.5.6

  * debian/control
    - Set Standards-Version: 4.2.0 (no change)
  * debian/copyright
    - Update copyright year
  * debian/.gitlab-ci.yml
    - Add .gitlab-ci.yml to run Continuous Build

 -- Hideki Yamane <henrich@debian.org>  Mon, 13 Aug 2018 16:39:35 +0900

snapper (0.5.4-4) unstable; urgency=medium

  [ Dimitri John Ledkov ]
  * Update build-dependency on the new libbtrfs-dev (Closes: #898877)

 -- Hideki Yamane <henrich@debian.org>  Thu, 17 May 2018 11:34:49 +0900

snapper (0.5.4-3) unstable; urgency=medium

  * debian/rules
    - Fix FTCBFS (Closes: #894966)
      Thanks to Helmut Grohne <helmut@subdivi.de> for the patch
  * debian/patches
    - Properly add 0003-Add-debian-specific-option-DISABLE_APT_SNAPSHOT.patch 
      for Bug#880144
  * Revert changes in 0.5.4-2 except debian/rules

 -- Hideki Yamane <henrich@debian.org>  Fri, 06 Apr 2018 12:16:19 +0900

snapper (0.5.4-2) unstable; urgency=medium

  * debian/80snapper
    - Add option to disable apt snapshotting (Closes: #880144)
      Thanks to James Valleroy <jvalleroy@mailbox.org> for the patch
  * debian/default
    - Add it as /etc/default/snapper
  * debian/control
    - Add Build-Depends: dh-exec
  * debian/snapper.install
    - Use dh-exec and add debian/default
  * debian/rules
    - Use dh_missing instead of dh_install --fail-missing

 -- Hideki Yamane <henrich@debian.org>  Thu, 05 Apr 2018 21:58:58 +0900

snapper (0.5.4-1) unstable; urgency=medium

  * New upstream release
  * debian/patches
    - drop 0003-fix-888535-set-sysconfig-dir-in-manpage-correctly.patch:
      merged upstream

 -- Hideki Yamane <henrich@debian.org>  Thu, 01 Feb 2018 22:11:03 +0900

snapper (0.5.0-4) unstable; urgency=medium

  * debian/patches
    - add 0003-fix-888535-set-sysconfig-dir-in-manpage-correctly.patch
      Thanks to Daniel Mulholland <dan.mulholland@gmail.com> for the report
      (Closes: #888535) 

 -- Hideki Yamane <henrich@debian.org>  Sun, 28 Jan 2018 02:00:55 +0900

snapper (0.5.0-3) unstable; urgency=medium

  * debian/control
    - drop dh-autoreconf with dh10
    - set Build-Depends: debhelper (>= 11) 
    - set Standards-Version: 4.1.3
    - libsnapper4: set "Multi-Arch: same"
    - move Vcs-* to salsa.debian.org
  * debian/compat
    - set 11
  * debian/rules
    - add "--disable-ext4" option to explicitly disable experimental
      ext4 support (Closes: #887292)

 -- Hideki Yamane <henrich@debian.org>  Fri, 26 Jan 2018 23:11:15 +0900

snapper (0.5.0-2) unstable; urgency=medium

  * debian/control
    - set Standards-Version: 4.0.0
    - s/btrfs-tools/btrfs-progs/ since -tools is transitional dummy package
  * debian/rules
    - drop unnecessary autoreconf option with dh10
  * debian/watch
    - update to version 4

 -- Hideki Yamane <henrich@debian.org>  Wed, 19 Jul 2017 19:59:39 +0900

snapper (0.5.0-1) unstable; urgency=medium

  * New upstream release 

 -- Hideki Yamane <henrich@debian.org>  Sat, 13 May 2017 12:07:32 +0900

snapper (0.4.1-3) unstable; urgency=medium

  * debian/control
    - add missing dependency for libsnapper-dev (Closes: #856945)
      Thanks to Andreas Beckmann <anbe@debian.org> 

 -- Hideki Yamane <henrich@debian.org>  Tue, 07 Mar 2017 23:32:58 +0900

snapper (0.4.1-2) unstable; urgency=medium

  * debian/patches
    - add 0002-Bug-852574-Update-udevadm-path-to-bin-udevadm.patch
      Thanks to Michael Biebl <biebl@debian.org> (Closes: #852574) 

 -- Hideki Yamane <henrich@debian.org>  Sun, 19 Feb 2017 23:38:08 +0900

snapper (0.4.1-1) unstable; urgency=medium

  * New upstream release 
  * debian/patches
    - drop 0002-fix-typo.patch: merged upstream
  * rename debian/libsnapper3.install to debian/libsnapper4.install
  * debian/control
    - s/libsnapper3/libsnapper4/

 -- Hideki Yamane <henrich@debian.org>  Thu, 22 Dec 2016 20:14:18 +0900

snapper (0.3.3-4) unstable; urgency=medium

  * polish debian/80snapper 

 -- Hideki Yamane <henrich@debian.org>  Fri, 04 Nov 2016 07:00:27 +0900

snapper (0.3.3-3) unstable; urgency=medium

  * fix regression for debian/80snapper

 -- Hideki Yamane <henrich@debian.org>  Fri, 07 Oct 2016 15:36:44 +0900

snapper (0.3.3-2) unstable; urgency=medium

  * debian/control
    - add Enhances: btrfs-tools 
  * debian/80snapper
    - more check to run snapper
    - add cleanup algorism 

 -- Hideki Yamane <henrich@debian.org>  Sat, 01 Oct 2016 21:18:16 +0900

snapper (0.3.3-1) unstable; urgency=medium

  * debian/control
    - set New maintainer
    - set debhelper (>= 10)
  * debian/snapper.{preinst,postinst,postrm}
    - set error check 
  * debian/rules
    - make the build reproducible (Closes: #818027)
      Thanks to Sascha Steinbiss <satta@debian.org> for the patch.
  * debian/libsnapper-dev.examples
    - avoid to install .gitignore files
  * debian/patches
    - add 0002-fix-typo.patch
  * debian/copyright
    - remove unused ltmain.sh and gpl-2+ with libtool exception lines

 -- Hideki Yamane <henrich@debian.org>  Mon, 12 Sep 2016 21:35:10 +0900

snapper (0.3.3-0.4) experimental; urgency=medium

  * Non-maintainer upload.
  * debian/80snapper
    - avoid breaking apt behavior
  * update debian/bash-completion/snapper 

 -- Hideki Yamane <henrich@debian.org>  Fri, 09 Sep 2016 23:06:26 +0900

snapper (0.3.3-0.3) experimental; urgency=medium

  * Non-maintainer upload.
  * debian/80snapper
    - add apt hook snapshot (Closes: #770938)
      it's just hacky test implementation, so comments are welcome.
  * add debian/bash-completion/snapper

 -- Hideki Yamane <henrich@debian.org>  Sat, 03 Sep 2016 14:43:18 +0900

snapper (0.3.3-0.2) experimental; urgency=medium

  * Non-maintainer upload
  * fix to load systemd timer
  * debian/compat
    - set 10 to enable systemd service by default 
  * debian/control
    - use Build-Depends: debhelper (>= 9.20160709) to use dh-systemd function

 -- Hideki Yamane <henrich@debian.org>  Sat, 13 Aug 2016 17:03:53 +0900

snapper (0.3.3-0.1) experimental; urgency=medium

  * Non-maintainer upload
  * New upstream release
  * debian/control
    - add Build-Depends: libboost-test-dev
    - add Build-Depends: e2fslibs-dev
    - add Build-Depends: locales-all to deal with "FAIL: cmp-lt.test"
    - change from libsnapper2 to 3
    - use https for Vcs-*
    - set Standards-Version: 3.9.8
  * debian/patches
    - drop 0001-Link-boost-libraries-without-the-mt-suffix.patch: merged
    - refresh 0002-Add-DSO-linker-options-for-libsnapper.la.patch
  * debian/snapper.install
    - install systemd related files and {installation,systemd}-helper
      It also changes from cron to systemd timer job (Closes: #791726)
  * debian/rules
    - remove cron settings
    - enable hardening
  * debian/snapper.{preinst,postinst,postrm}
    - remove cron files

 -- Hideki Yamane <henrich@debian.org>  Mon, 08 Aug 2016 06:42:22 +0900

snapper (0.2.4-1) unstable; urgency=low

  * New upstream release (Closes: #763262)
    - Update copyright info
    - Add build-dependencies on libacl1-dev and libmount-dev
    - Refresh patches
  * Update Standards-Version to 3.9.6 (no changes)

 -- Nicolas Dandrimont <olasd@debian.org>  Mon, 13 Oct 2014 23:14:31 +0200

snapper (0.1.8-2) unstable; urgency=medium

  * Move PAM config to /usr/share/pam-configs/snapper for consistency with
  other pam module-providing packages (Closes: #734784)

 -- Nicolas Dandrimont <olasd@debian.org>  Thu, 09 Jan 2014 21:41:31 +0100

snapper (0.1.8-1) unstable; urgency=low

  * Initial release. (Closes: #715404)

 -- Nicolas Dandrimont <olasd@debian.org>  Sat, 07 Dec 2013 21:45:39 +0100
